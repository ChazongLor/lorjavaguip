This project you will build, using Java, two versions of an application 
which can open up a text document, count the total number of words, the total 
number of different words (ignoring case) and the count for each different word. 
This project should properly configure and use a java.util.Scanner object to scan through 
an input file and pull out the words while ignoring all the punctuation. 
This project should use a java.util.Hashmap object to track the words found.
The first version of this application will run on the command line, taking in 
the name of the input file. It will write out to a file a total count of the 
words, and then a count of each word found. The words may be in either 
alphabetical order or in frequency order. The five most popular words and 
five least popular words along with the total count should also be shown on 
standard out. The second version of this application will create a user 
interface using the Java Swing libraries. From the interface one can run the 
app, open a file with a file chooser, and similarly write the counts of all the 
words out to a file, while displaying the total count, the most popular 5 words, 
and the least popular 5 words in the GUI.


How to run the program:
1) Go to command line 
2) Head towards where the folder containing the files are at.
Example: C:\Users\<username>> cd Downloads
then C:\Users\<username>\Downloads> cd WordCounter&JavaGUI
3) Then type cd src/, which is where the .java files are at
4) Type javac Driver.java to compile the Driver class
5) Type java Driver hamlet.txt to run the program.
(Must have an argument)