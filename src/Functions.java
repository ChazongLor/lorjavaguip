/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Scanner;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java .util.List;
import java.io.File;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Collections;


/**
 *
 * @author lorcc
 */
public class Functions {
    
    // Used to keep track of the amount of words in the given file
    static int wordCount = 0;
    
    /**
     * Function will write results to a new file with the given name
     * @param hmap
     * @throws FileNotFoundException 
     */
    public static void WriteFile(Set<Map.Entry<String,Integer>> hmap) throws FileNotFoundException{
        String outputFile = "Output.txt";
        PrintWriter printWrite = new PrintWriter(outputFile);
        
        for(Map.Entry<String,Integer> mapSorted: hmap){
            printWrite.println(mapSorted.getKey() + " = " + mapSorted.getValue());
        }
        printWrite.println(Functions.Counter());
        printWrite.close();
    }
    
    /**
     * Creates a hash map that will put a new word into the map
     * The first instance of a word will be added into the hash map.
     * If a word appears more than once, it will not be added to the map
     * instead it will increase the word count.
     * @param file
     * @throws FileNotFoundException 
     */
    public static void HashMap(File file) throws FileNotFoundException{
        HashMap<String,Integer> map = new HashMap();
        Scanner scans = new Scanner(file);
        while(scans.hasNext()){
            String line = scans.next().toLowerCase().replaceAll("!", "").replaceAll(" ", "");
            String[] word = line.replaceAll("\\?", "").split("\\ *(\\:|\\;|,|\\.)\\ *");
            for(String words: word){
                if(map.containsKey(words)){
                    int valCount = map.get(words) + 1;
                    map.put(words, valCount);
                    wordCount++;
                }else{
                    map.put(words, 1);
                    wordCount++;
                }
            }
        }
        Functions.SortedHashMap(map);
        System.out.println(Functions.Counter());
    }
    
    /**
     * Will compare the entry values from hmap, it will then convert
     * the set into a list to get sorted, next will be put into a LinkedHashMap,
     * and then will put the sorted hmap into a new set, write out to file with
     * the words and the number of times that each individual word appears.
     * @param hmap
     * @throws FileNotFoundException 
     */
    public static void SortedHashMap(HashMap hmap) throws FileNotFoundException{
       //Comparator that takes in map entry value
       //it then compares and arranges them
       Set<Map.Entry<String,Integer>> entry = hmap.entrySet();
       Comparator<Map.Entry<String,Integer>> Comp = new Comparator<Map.Entry<String,Integer>>(){
           @Override
           public int compare(Map.Entry<String,Integer> w1, Map.Entry<String,Integer> w2 ){
               Integer str = w1.getValue();
               Integer str1 = w2.getValue();
               return str1.compareTo(str);
           }
       };
       
       //Converts the set into a list to get sorted
       List<Map.Entry<String,Integer>> ListEntry = new ArrayList<>(entry);
       Collections.sort(ListEntry, Comp);
       
       //A LinkedHashMap with the size of the entries
       LinkedHashMap<String,Integer> valSorted = new LinkedHashMap<String,Integer>(ListEntry.size());
       
       //Copies the entries from the list above and puts it into the map.
       for(Map.Entry<String,Integer> wEntry: ListEntry){
           valSorted.put(wEntry.getKey(), wEntry.getValue());
       }
       
       //Will put the sorted words into a set
       Set<Map.Entry<String,Integer>> entrySort = valSorted.entrySet();
       
       //Calls the function WriteFile to write to a file
       Functions.WriteFile(entrySort);
       
       //Prints out the sorted map
       for(Map.Entry<String,Integer> sortedMap: entrySort){
           System.out.println(sortedMap.getKey() + " = " + sortedMap.getValue());
       }
       
       //Prints the first five occuring words with the number of times
       Set<String> keys = valSorted.keySet();
       Collection<Integer> vals= valSorted.values();
       Integer[] valArr = vals.toArray(new Integer[vals.size()]);
       String[] keyArr = keys.toArray(new String[keys.size()]);
       System.out.println("\nFirst five occuring words: ");
       for(int i = 0; i < keyArr.length && i < 5; i++){
           System.out.println(keyArr[i] + " = " + valArr[i]);
       }
        
       //Print the last five occuring words with the number of times
       System.out.println("\nLast five occuring words: ");
       for(int i = keys.size() - 1; i > keys.size() - 6; i--){
           System.out.println(keyArr[i] + " = " + valArr[i]);
       }
    }
    
    /**
     * Function will return the total words that are in file.
     * @return Total word count
     */
    public static String Counter(){
        String results = "Final Word Count: " + wordCount;
        return results;
    }
}
