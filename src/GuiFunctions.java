/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lorcc
 */
public class GuiFunctions extends JFrame implements ActionListener {
    
    //preferred width of the text box, does not limit amount of characters
    private JTextField text = new JTextField(50);
    
    private JButton fileButton = new JButton("What file to sort?");
    
    public GuiFunctions(){
        this.setTitle("Word Counter");
        //x-coor = 300, y-coor = 400, width = 350, height = 250
        this.setBounds(300, 400, 350, 250);
        this.getContentPane().setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.fileButton.setBounds(90, 90, 150, 30);
        this.getContentPane().add(fileButton);
        this.fileButton.addActionListener(this);
    }
    
    private void FileButtonPushed() throws FileNotFoundException{
        JFileChooser choose = new JFileChooser();
        choose.setFileFilter(new FileNameExtensionFilter("TEXT FILES" , "txt", "text"));
        int chooseSuccess = choose.showOpenDialog(null);
        if(chooseSuccess == JFileChooser.APPROVE_OPTION){
            File chosenFile = choose.getSelectedFile();//Pass file to function
            Functions.HashMap(chosenFile);
            System.out.println(Functions.Counter());
        }else{
            System.out.println("You hit cancel.");
        }
    }
    
    public void actionPerformed(ActionEvent ae){
        if(ae.getActionCommand().equals("What file to sort?")){
            try{
                this.FileButtonPushed();
            }catch (FileNotFoundException ex){
                Logger.getLogger(GuiFunctions.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
